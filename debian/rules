#!/usr/bin/make -f
# -*- makefile -*-

# This has to be exported to make some magic below work.
export DH_OPTIONS

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

STATIC_BUILD_PATH=DEB_build_static
SHARED_BUILD_PATH=DEB_build_shared

# TODO: possibly consider enabling CIFTI code -- needed by AFNI: -DUSE_CIFTI_CODE=ON \

CMAKE_FLAGS=-DCMAKE_INSTALL_PREFIX:PATH=/usr \
            -DNIFTI_INSTALL_INCLUDE_DIR:PATH=include/nifti \
            -DBUILD_SHARED_LIBS=ON \
            -DCMAKE_C_FLAGS:STRING="$$CFLAGS" \
            -DCMAKE_SHARED_LINKER_FLAGS="$$LDFLAGS -Wl,--as-needed" \
            -DCMAKE_EXE_LINKER_FLAGS="$$LDFLAGS -Wl,--as-needed" \
            -DUSE_FSL_CODE:BOOL=OFF \
            -DUSE_CIFTI_CODE=OFF \
            -DNIFTI_BUILD_TESTING:BOOL=OFF

# one ring to rule them all ...
%:
	dh $@ --buildsystem=cmake

override_dh_auto_clean:
	rm -rf $(STATIC_BUILD_PATH) $(SHARED_BUILD_PATH)
	rm -rf docs/html
	dh_clean

override_dh_auto_configure:
	if [ ! -d $(STATIC_BUILD_PATH) ]; then mkdir $(STATIC_BUILD_PATH); fi
	if [ ! -d $(SHARED_BUILD_PATH) ]; then mkdir $(SHARED_BUILD_PATH); fi
	cd $(STATIC_BUILD_PATH) \
	    && cmake $(CURDIR) $(CMAKE_FLAGS) -DBUILD_SHARED_LIBS:BOOL=OFF \
	                                      -DBUILD_TESTING:BOOL=ON
	cd $(SHARED_BUILD_PATH) \
	    && cmake $(CURDIR) $(CMAKE_FLAGS) -DBUILD_SHARED_LIBS:BOOL=ON \
	                                      -DBUILD_TESTING:BOOL=OFF

override_dh_auto_build:
	# build static libs
	cd $(STATIC_BUILD_PATH) && $(MAKE) VERBOSE=1
	# build shared libs and binaries
	cd $(SHARED_BUILD_PATH) && $(MAKE) VERBOSE=1
	# build api reference
	cd docs && doxygen Doxy_nifti.txt
	# remove jquery (use from package dependency)
	rm -f docs/html/jquery.js

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	cd $(STATIC_BUILD_PATH) && ctest
endif

override_dh_auto_install:
	# install binaries and shared libs
	cd $(SHARED_BUILD_PATH) && $(MAKE) install DESTDIR=$(CURDIR)/debian/tmp
	# install static libs
	cd $(STATIC_BUILD_PATH)/znzlib \
		&& $(MAKE) install DESTDIR=$(CURDIR)/debian/tmp
	cd $(STATIC_BUILD_PATH)/niftilib \
		&& $(MAKE)	install DESTDIR=$(CURDIR)/debian/tmp
	cd $(STATIC_BUILD_PATH)/nifticdf \
		&& $(MAKE)	install DESTDIR=$(CURDIR)/debian/tmp
	cp -a DEB_build_static/*/lib*.a debian/tmp/usr/lib
	# install manpage
	$(STATIC_BUILD_PATH)/utils/nifti_tool -help | \
		python3 debian/makeniftitoolmanpage.py > debian/tmp/nifti_tool.1
	# Remove unecessary header file
	rm -f $(CURDIR)/debian/tmp/usr/include/nifti/nifti_tool.h
	d-shlibmove --commit \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --override s/libznz[0-9.]*-dev/libznz-dev/ \
		    --movedev debian/tmp/usr/include/* usr/include \
		    debian/tmp/usr/lib/libnifti*.so
	d-shlibmove --commit \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --movedev debian/libnifti2-dev/usr/include/nifti/znzlib.h usr/include/nifti \
		    debian/tmp/usr/lib/libznz*.so
	d-shlibmove --commit \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --override s/libznz[0-9.]*-dev/libznz-dev/ \
		    --movedev debian/libnifti2-dev/usr/include/nifti/nifti1_io.h usr/include/nifti \
		    --movedev debian/libnifti2-dev/usr/include/nifti/nifti2_io.h usr/include/nifti \
		    debian/tmp/usr/lib/libniftiio*.so
	d-shlibmove --commit \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --override s/libznz[0-9.]*-dev/libznz-dev/ \
		    --movedev debian/libnifti2-dev/usr/include/nifti/nifticdf.h usr/include/nifti \
		    debian/tmp/usr/lib/libnifticdf*.so
	find debian -name "lib*.la" -delete

override_dh_installchangelogs:
	dh_installchangelogs -k Updates.txt
