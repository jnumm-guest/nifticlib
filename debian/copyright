Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nifticlib
Upstream-Source: https://github.com/NIFTI-Imaging/nifti_clib/releases/

Files: *
Copyright: none
License: other
  nifticlib was written by the NIfTI Data Format Working Group (DFWG,
  http://nifti.nimh.nih.gov/). The NIFTI DFWG disclaims all
  copyright in nifticlib and places it in the public domain.

Files: fsliolib/*
Copyright: none
License: other
  Upstream version 0.4 changed the copyright statements of all source files that
  were developed as part of FSL (http://www.fmrib.ox.ac.uk/fsl/) to make
  the package acceptable for Debian main. FSL itself is published under a
  non-free license. The original authors, who are also part of the nifticlib
  upstream team, disclaimed there copyright in those files.
 .
  From a request on the debian-legal mailing list it arose that the new
  copyright statements should be acceptable for Debian main, but there might be
  difficulties for upstream to place this software into the public domain.
 .
  http://lists.debian.org/debian-legal/2006/09/msg00026.html
 .
  Please note, that all code is simply left in the source tarball, but is not
  compiled and hence not part of the binary packages.


Files: fsliolib/dbh.h
Copyright: (c) Copyright 1986-1995, Biomedical Imaging Resource, Mayo
  Foundation. Incorporation
License: other
  Use of the ANALYZE 7.5 file header by permission of the Mayo Foundation.
  Changes from the ANALYZE 7.5 file header in this file are released to the
  public domain, including the functional comments and any amusing asides.


Files: debian/*
Copyright: Copyright 2006-2008, Michael Hanke <michael.hanke@gmail.com>
License: Expat
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:
 .
  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.
 .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
